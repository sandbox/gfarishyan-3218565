<?php

namespace Drupal\webform_entity_embed\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;
use Drupal\embed\EmbedCKEditorPluginBase;

/**
 * Defines the "drupalWebforms" plugin.
 *
 * @CKEditorPlugin(
 *   id = "drupalwebforms",
 *   label = @Translation("Webform"),
 *   embed_type_id = "embed_webforms"
 * )
 */
class DrupalWebforms extends EmbedCKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'webform_entity_embed') . '/js/plugins/drupalwebforms/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'DrupalWebforms_dialogTitleAdd' => t('Insert webforms'),
      'DrupalViews_dialogTitleEdit' => t('Edit webforms'),
      'DrupalWebforms_buttons' => $this->getButtons(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/jquery',
      'core/drupal',
      'core/drupal.ajax',
    ];
  }

}
