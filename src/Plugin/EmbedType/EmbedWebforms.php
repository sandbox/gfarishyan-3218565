<?php

namespace Drupal\webform_entity_embed\Plugin\EmbedType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\webform\WebformInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\embed\EmbedType\EmbedTypeBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Viws embed type.
 *
 * @EmbedType(
 *   id = "embed_webforms",
 *   label = @Translation("Webforms")
 * )
 */
 
class EmbedWebforms extends EmbedTypeBase implements ContainerFactoryPluginInterface {
 
  protected $entityTypeManager;
  
  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, 
      $plugin_id, 
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'filter_webforms' => FALSE,
      'webform_options' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultIconUrl() {
    return file_create_url(drupal_get_path('module', 'views_entity_embed') . '/js/plugins/drupalviews/views_entity_embed.png');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['filter_webforms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter which Webforms to be allowed as options:'),
      '#default_value' => $this->getConfigurationValue('filter_webforms'),
    ];
    $form['webform_options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed Webforms'),
      '#default_value' => $this->getConfigurationValue('webform_options'),
      '#options' => $this->getAllWebforms(),
      '#states' => [
        'visible' => [':input[name="type_settings[filter_webforms]"]' => ['checked' => TRUE]],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!$form_state->hasAnyErrors()) {
      $this->setConfigurationValue('filter_webforms', $form_state->getValue('filter_webforms'));
      // Set views options.
      $webform_options = $form_state->getValue('filter_webforms') ? array_filter($form_state->getValue('webform_options')) : [];
      $this->setConfigurationValue('webform_options', $webform_options);
    }
  }

  /**
   * Methods get all webforms as options list.
   */
  protected function getAllWebforms() {
    $webforms = [];
    
    foreach ($this->entityTypeManager->getStorage('webform')->loadMultiple(NULL) as $webform) {
      $webforms[$webform->id()] = $webform->label();
    }
    
    return $webforms;
  }
}
