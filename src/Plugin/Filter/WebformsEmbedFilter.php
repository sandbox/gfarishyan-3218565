<?php

namespace Drupal\webform_entity_embed\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\entity_embed\Exception\EntityNotFoundException;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\embed\DomHelperTrait;
use Drupal\views\Views;
use Drupal\Component\Serialization\Json;

/**
 * Provides a filter to display embedded entities based on data attributes.
 *
 * @Filter(
 *   id = "webforms_embed",
 *   title = @Translation("Display embedded Webforms"),
 *   description = @Translation("Embeds webforms using data attributes: data-webform-name."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE
 * )
 */
class WebformsEmbedFilter extends FilterBase implements ContainerFactoryPluginInterface {

  use DomHelperTrait;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a ViewsEmbedFilter object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration, $plugin_id, $plugin_definition, $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    if (strpos($text, 'data-webform-name') !== FALSE) {
      $dom = Html::load($text);
      $xpath = new \DOMXPath($dom);

      foreach ($xpath->query('//drupal-webforms[@data-webform-name]') as $node) {
        $render_view = '';
        try {
          $build = $this->buildViewEmbed($node);
          $render_view = $this->renderer->executeInRenderContext(new RenderContext(), function () use (&$build) {
            return $this->renderer->render($build);
          });
          $result = $result->merge(BubbleableMetadata::createFromRenderArray($build));
        }
        catch (\Exception $e) {
          throw new EntityNotFoundException(sprintf('Unable to load embedded %s view with %s display.', $view_name, $view_display));
        }

        $this->replaceNodeContent($node, $render_view);
      }
      $result->setProcessedText(Html::serialize($dom));
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return $this->t('
        <p>You can embed Webforms. Example:</p>
        <code><drupal-webform data-webform-name="mywebform_id"></drupal-webform></code>');
    }
    else {
      return $this->t('You can embed Webforms.');
    }
  }

  /**
   * Method that build data attributes per node.
   */
  protected function buildViewEmbed($node) {
    $webform_name = $node->getAttribute('data-webform-name');
    // Ensure that views default executions are run. These ensure that view
    // display settings such as "use_ajax" are actually read and respected.
    $build = [
       '#type' => 'webform',
       '#webform' => $webform_name,
       '#context' => [
        'data-webform-name' => $webform_name,
       ],
     ];
    return $build;
  }

}
