<?php

namespace Drupal\webform_entity_embed\Form;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\SetDialogTitleCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\editor\EditorInterface;
use Drupal\embed\EmbedButtonInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a form to embed Webform.
 */
class WebformEmbedDialog extends FormBase {

  /**
   * The entitytype  manager.
   *
   * @var \Drupal\entity_embed\EntityEmbedDisplay\EntityTypeManagerInterface
   */

  protected $entityTypeManager;

  /**
   * The entity embed display manager.
   *
   * @var \Drupal\entity_embed\EntityEmbedDisplay\EntityEmbedDisplayManager
   */
  protected $entityEmbedDisplayManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_entity_embed_dialog';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(FormBuilderInterface $form_builder, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entityTypeManager) {
    $this->formBuilder = $form_builder;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'), 
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EditorInterface $editor = NULL, EmbedButtonInterface $embed_button = NULL) {

    $form_state->set('embed_button', $embed_button);
    $form_state->set('editor', $editor);
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="webforms-entity-embed-dialog-form">';
    $form['#suffix'] = '</div>';
    
    $view_element = $form_state->get('webform_element');
    $filterByWebforms = $this->getWebformOptions($embed_button);
    $form['webform_name'] = [
      '#type' => 'select',
      '#options' => $filterByWebforms,
      '#title' => t('Select Webform'),
      '#required' => TRUE,
      '#default_value' => isset($view_element['data-webform-name']) ? $view_element['data-webform-name'] : '',
    ];

    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Embed'),
      '#button_type' => 'primary',
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitEmbedWebform',
        'event' => 'click',
      ],
      '#attributes' => [
        'class' => [
          'js-button',
        ],
      ],
    ];
    $form['#attributes']['class'][] = 'webforms-entity-embed-dialog';
    return $form;
  }
  
  public function submitEmbedWebform(array &$form, FormStateInterface$form_state) {
    $response = new AjaxResponse();
    
    $embed_button = $form_state->get('embed_button');
    $webform_name = $form_state->getValue('webform_name');
    
    $view_element = [
      'data-webform-name' => $webform_name,
      'data-embed-button' => $embed_button->id(),
    ];
    
    $response->addCommand(new EditorDialogSave(['attributes' => $view_element]));
    $response->addCommand(new CloseModalDialogCommand());
    
    return $response;
  }

  /**
   * Get all Views as options.
   */
  protected function getWebformOptions($embed_button) {
    $webforms = ['' => $this->t('Select Webform')];
    foreach ($this->entityTypeManager->getStorage('webform')->loadMultiple() as $webform) {
      $webforms[$webform->id()] = $webform->label();
    }
    
    return $embed_button->getTypeSetting('filter_webforms') ? array_intersect_key($webforms, $embed_button->getTypeSetting('webforms_options')) : $webforms;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
