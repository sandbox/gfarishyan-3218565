Webform Entity Embed allows you to embed Webforms in textarea using WYSIWYG editor.

Installation

Download module and store it in module folder or use composer ( composer require drupal/webform_entity_embed) .
Enable the module
Enable the filter 'Display embedded Webforms' for the desired text formats from the Text formats and editors configuration page. ( admin/config/content/formats )
If the Limit allowed HTML tags filter is enabled, add to the Allowed HTML tags.
Go to Embed buttons administration page (/admin/config/content/embed) and create a new button, which embedded type is 'Webform'.
You can choose Which webforms are allowed to embed.
Only the selected webforms will be allowed to be embed by this Webform embed button. If you leave the options unchecked so all the webforms will be allowed.

To enable the WYSIWYG plugin, move the webforms-entity-embed button into the Active toolbar for the desired text formats.
In WYSIWYG after click the Webforms Embed Button.

